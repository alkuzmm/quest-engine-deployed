package ua.ithillel.evo.questengine.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.evo.questengine.data.entity.Hint;

@Repository
public interface HintRepository extends CrudRepository<Hint, Long> {
}
